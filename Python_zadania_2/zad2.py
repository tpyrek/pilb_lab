#!/usr/bin/python3


class ComplexNumber:
    '''
    Klasa reprezentująca liczny zespolone
    '''
    def __init__(self, re , im):
        '''
        Konstruktor liczby zespolonej
        :param re: część rzeczywista
        :param im: część urojona
        '''
        self.re = re
        self.im = im

    def __abs__(self):
        return (self.re ** 2 + self.im ** 2) ** (1/2)

    def __add__(self, complex_number):
        return ComplexNumber(self.re + complex_number.re, self.im+complex_number.im)

    def __sub__(self, complex_number):
        return ComplexNumber(self.re - complex_number.re, self.im - complex_number.im)

    def __truediv__(self, complex_number):
        if complex_number.re and complex_number.im == 0:
            return

        a = self.re
        b = self.im
        c = complex_number.re
        d = complex_number.im

        return ComplexNumber((a * c + b * d)/(c ** 2 + d ** 2), (b * c - a * d)/(c ** 2 + d ** 2))

    def __mul__(self, complex_number):

        a = self.re
        b = self.im
        c = complex_number.re
        d = complex_number.im

        return ComplexNumber((a * c - b * d), (a * d + b * c))

    def __str__(self):
        temp = str(self.im)
        if self.im >= 0:
            temp = "+" + temp

        return str(self.re) + temp + "i"


# TESTS

if __name__ == "__main__":
    a = ComplexNumber(1,4)
    b = ComplexNumber(2,4)
    c = b / a
    d = b + a
    e = b - a
    f = b * a
    g = abs(a)

    print(c)
    print(d)
    print(e)
    print(f)
    print(g)

    b /= a

    print(b)

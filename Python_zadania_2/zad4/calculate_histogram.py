import threading
from time import sleep


class CalculateHistogram(threading.Thread):
    def __init__(self, queue, bins):
        threading.Thread.__init__(self)
        self.queue = queue
        self.run_thread = True
        self.bins = bins
        self.array = []

    def run(self):

        while self.run_thread:
            if not self.queue.empty():
                temp = self.queue.get()
                self.queue.task_done()
                self.array.append(temp)

        for tab in self.array:
            self.bins += tab

        print("Tablica obliczonych wartości histogramu: ")
        print(self.bins)

    def end(self):

        while not self.queue.empty():
            sleep(0.1)

        self.run_thread = False

#!/usr/bin/python3

from queue import Queue
import cv2
import numpy
import matplotlib.pyplot as plt
from get_data_from_image import GetDataFromImage
from calculate_histogram import CalculateHistogram


def main():

    queue = Queue()
    threads = []

    image = cv2.imread("test.jpg")
    height, width, channels = image.shape
    bins = numpy.zeros(256, numpy.int32)

    number_of_threads = 5
    height_resolution = int(height / number_of_threads)

    for i in range(0, number_of_threads-1):
        threads.append(GetDataFromImage(queue, image, i * height_resolution, (i+1) * height_resolution, width))

    threads.append(GetDataFromImage(queue, image, (number_of_threads-1)*height_resolution, height, width))

    calculate_histogram_thread = CalculateHistogram(queue, bins)
    calculate_histogram_thread.start()

    for thread in threads:
        thread.setDaemon(True)
        thread.start()

    for thread in threads:
        thread.join()

    print("Joined")
    calculate_histogram_thread.end()

    calculate_histogram_thread.join()
    print("Koniec")

    plt.bar(numpy.arange(len(bins)), bins)
    plt.show()


if __name__=="__main__":
    main()
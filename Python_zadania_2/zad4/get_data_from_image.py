import threading
import numpy


class GetDataFromImage(threading.Thread):
    def __init__(self, queue, image, row_start, row_end, image_width):
        threading.Thread.__init__(self)
        self.queue = queue
        self.image = image
        self.row_start = row_start
        self.row_end = row_end
        self.image_width = image_width
        self.bins = numpy.zeros(256, numpy.int32)

    def run(self):

        for i in range(self.row_start, self.row_end):   #height
            for j in range(0, self.image_width):

                intensity = 0

                for value in self.image[i][j]:
                    intensity += value

                self.bins[int(intensity/3)] += 1

            print("Row: " + str(i))

        self.queue.put(self.bins)

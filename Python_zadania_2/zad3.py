#!/usr/bin/python3

import xml.dom.minidom
import xml.sax

# DOM
print("DOM: \n\n")
document = xml.dom.minidom.parse("test.xml")

for node_breakfast in document.getElementsByTagName("breakfast"):
    for food_node in node_breakfast.getElementsByTagName("food"):
        name = food_node.getElementsByTagName("name")[0].firstChild
        price = food_node.getElementsByTagName("price")[0].firstChild
        print(name.data + " " + price.data)
        name.replaceWholeText(name.data + "1")

file = open("test_converted.xml", "w", encoding='utf-8')
document.writexml(file)
file.close()

# SAX
print("\n\nSAX: \n\n")


class FoodHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.CurrentData = ""
        self.name = ""
        self.price = ""

    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if self.CurrentData == "food":
            print("FOOD: ")

    def characters(self, content):
        if self.CurrentData == "name":
            self.name = content
        elif self.CurrentData == "price":
            self.price = content

    def endElement(self, tag):
        if self.CurrentData == "name":
            print("Name: ", self.name)
        elif self.CurrentData == "price":
            print("Price :", self.price)
        self.CurrentData = ""


parser = xml.sax.make_parser()
food_handler = FoodHandler()
parser.setContentHandler(food_handler)
parser.parse("test.xml")

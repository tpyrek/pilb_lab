import threading
from time import sleep


class Philosopher(threading.Thread):
    '''
    Klasa reprezentująca filozofa
    '''
    def __init__(self, id, sticks, spaghetti_portions):
        '''
        Konstruktor
        :param id: unikalny nymer ID
        :param sticks: tablica 'widelców' czyli obiektów threading.Lock
        :param spaghetti_portions: ilość porcji spaghetti
        '''
        threading.Thread.__init__(self)
        self.id = id
        self.sticks = sticks
        self.spaghetti_portions = spaghetti_portions

    def run(self):
        self.run_dead_clock()

    def run_dead_clock(self):
        stick_index_1 = self.id
        stick_index_2 = (self.id + 1) % len(self.sticks)

        print("Filozof " + str(self.id) + " czeka na lewy widelec")
        self.sticks[stick_index_1].acquire()
        sleep(0.1)
        print("Filozof " + str(self.id) + " wziął lewy widelec")
        print("Filozof " + str(self.id) + " czeka na prawy widelec")
        self.sticks[stick_index_2].acquire()
        sleep(0.1)
        print("Filozof " + str(self.id) + " wziął prawy widelec")
        print("Filozof " + str(self.id) + " je")
        self.spaghetti_portions = self.spaghetti_portions - 1
        self.sticks[stick_index_2].release()
        print("Filozof " + str(self.id) + " odłożył prawy widelec")
        self.sticks[stick_index_1].release()
        print("Filozof " + str(self.id) + " odłożył lewy widelec")

        sleep(0.2)
        if self.spaghetti_portions > 0:
            self.run()

    def run_no_dead_clock(self):
        stick_index_1 = self.id
        stick_index_2 = (self.id + 1) % len(self.sticks)

        print("Filozof " + str(self.id) + " próbuje wziąć lewy widelec")
        if self.sticks[stick_index_1].acquire(False):
            sleep(0.1)
            print("Filozof " + str(self.id) + " wziął lewy widelec")
            print("Filozof " + str(self.id) + " próbuje wziąć prawy widelec")
            if self.sticks[stick_index_2].acquire(False):
                sleep(0.1)
                print("Filozof " + str(self.id) + " wziął prawy widelec")
                print("Filozof " + str(self.id) + " je")
                self.spaghetti_portions = self.spaghetti_portions - 1
                self.sticks[stick_index_2].release()
                print("Filozof " + str(self.id) + " odłożył prawy widelec")
                self.sticks[stick_index_1].release()
                print("Filozof " + str(self.id) + " odłożył lewy widelec")
            else:
                self.sticks[stick_index_1].release()
                print("Filozof " + str(self.id) + " odłożył lewy widelec")

        sleep(0.2)
        if self.spaghetti_portions > 0:
            self.run()

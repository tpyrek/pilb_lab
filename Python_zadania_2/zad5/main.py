#!/usr/bin/python3

import threading
from zad5 import Philosopher

def main():
    number_of_philosophers = 5
    sticks = []
    philosophers = []

    for i in range(0, number_of_philosophers):
        sticks.append(threading.Lock())

    for i in range(0, number_of_philosophers):
        philosophers.append(Philosopher(i, sticks, 2))

    for p in philosophers:
        p.setDaemon(True)
        p.start()

    for p in philosophers:
        p.join()

    print("Koniec")


if __name__ == "__main__":
    main()
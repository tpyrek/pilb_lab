#!/usr/bin/python3


if __name__ == "__main__":
    dictionary = {'i': 'oraz', 'oraz': 'i', 'nigdy': 'prawie nigdy', 'dlaczego': 'czemu'}

    file = open('test.txt')
    try:
        text = file.read()
    finally:
        file.close()

    print('Oryginalny tekst: ')
    print(text)

    text_words = text.split()
    processed_text = [word if word.lower() not in dictionary else dictionary[word] for word in text]
    processed_text = ' '.join(text)

    print('Przerobiony teskt:')
    print(processed_text)

    file = open('test_zmieniony.txt', 'w')
    file.write(processed_text)
    file.close()
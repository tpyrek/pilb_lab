#!/usr/bin/python3

import random
import numpy

if __name__ == "__main__":
    matrix_dimension = random.randint(0,50)
    matrix = numpy.matrix([[random.randint(0, 100) for i in range(0, matrix_dimension)] for i in range(0, matrix_dimension)])

    det = numpy.linalg.det(matrix)
    print(matrix)
    print("Wyznacznik: ")
    print(det)

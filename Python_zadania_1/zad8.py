#!/usr/bin/python3

import random

if __name__ == "__main__":
	number_list = random.sample(range(1, 100), 50)
	print("Przed sortowaniem: ")
	print(number_list)

	for i in range(0,  len(number_list)):
		for j in range(i+1, len(number_list)):
			if number_list[i] < number_list[j]:
				temp = number_list[i]
				number_list[i] = number_list[j]
				number_list[j]=temp
	print("Po sortowaniu: ")
	print(number_list)

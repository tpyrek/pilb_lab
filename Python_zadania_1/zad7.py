#!/usr/bin/python3


if __name__ == "__main__":
	print("Podaj a b c: ")
	a, b, c = input().split()
	a = int(a)
	b = int(b)
	c = int(c)

	delta = b**2-4*a*c

	if delta > 0:
		x1 = (-b-delta**(1/2))/(2*a)
		x2 = (-b+delta**(1/2))/(2*a)

		print("x1 = " + str(x1))
		print("x2 = " + str(x2))
	elif delta == 0:

		x = -b/(2*a)
		print("x = " + str(x))
	else:
		print("Nie ma pierwsiatkow")

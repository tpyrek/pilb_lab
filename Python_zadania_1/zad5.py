#!/usr/bin/python3

import os
start_path = "/dev"


def fun(path):
	dev_list = os.listdir(path)
	for i in dev_list:
		if os.path.isdir(path+"/"+i):
			fun(path+"/"+i)
		print(path+"/"+i)


if __name__ == "__main__":
	fun(start_path)

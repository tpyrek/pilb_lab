#!/usr/bin/python3

if __name__ == "__main__":
    dictionary = {'i': 'oraz', 'oraz': 'i', 'nigdy': 'prawie nigdy', 'dlaczego': 'czemu'}

    input_string = input('Wpisz tekst: ')
    print('Oryginalny tekst: ')
    print(input_string)

    input_string_words = input_string.split()
    processed_input_string = [word if word.lower() not in dictionary else dictionary[word] for word in input_string_words]
    processed_input_string = ' '.join(processed_input_string)

    print('Przerobiony teskt:')
    print(processed_input_string)

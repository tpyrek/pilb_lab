#!/usr/bin/python3

import numpy
import random


if __name__ == "__main__":
    matrix_a = numpy.matrix([[random.randint(0, 100) for i in range(0, 8)] for i in range(0, 8)])
    matrix_b = numpy.matrix([[random.randint(0, 100) for i in range(0, 8)] for i in range(0, 8)])
    print(matrix_a)
    print(matrix_b)

    matrix_a_b = matrix_a * matrix_b
    print(matrix_a_b)

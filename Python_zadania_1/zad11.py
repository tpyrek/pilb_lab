#!/usr/bin/python3

import numpy

if __name__ == "__main__":
    a = [1, 2, 12, 4]
    b = [2, 4, 2, 8]

    scalar_product = numpy.dot(a, b)
    print(scalar_product)

#!/usr/bin/python3

if __name__ == "__main__":
    words_to_remove = ['się', 'i', 'oraz', 'nigdy', 'dlaczego']
    input_string = input('Wpisz tekst: ')
    print('Oryginalny tekst: ')
    print(input_string)

    input_string_words = input_string.split()
    processed_input_string = [word for word in input_string_words if word.lower() not in words_to_remove]
    processed_input_string = ' '.join(processed_input_string)

    print('Przerobiony teskt:')
    print(processed_input_string)
